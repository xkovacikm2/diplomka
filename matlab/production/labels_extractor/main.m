label_directories = dir('/home/kovko/Documents/FIIT/ING/diplomovka/labels');
%remove . and ..
label_directories = label_directories(3:end)

organs = ["oesophagus", "cor", "pulmo_sin", "pulmo_dex", "medulla_spinalis"];

directories_size = size(label_directories);

for dir_idx = 1:1:directories_size(1)
  directory = label_directories(dir_idx);
  study_dir = strcat(directory.folder, '/', directory.name);

  for organ_idx = 0:1:4
    organ_raw_labels = strcat(study_dir, '/', '1 RTSTRUCT RTstruct.seg/1 RTSTRUCT RTstruct.seg_', num2str(organ_idx), '.vtp');
    points = vtp2points(organ_raw_labels);

    filename = strcat(study_dir, '/', organs(organ_idx + 1), '.mat')
    save(filename, 'points');
  end
end
