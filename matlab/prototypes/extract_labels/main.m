xml_data = xml2struct('/home/kovko/Documents/FIIT/ING/diplomovka/matlab_proto/seg_data/1 RTSTRUCT RTstruct/1 RTSTRUCT RTstruct_3.vtp');
raw_points = xml_data.VTKFile.PolyData.Piece.Points.DataArray.Text;
words = strsplit(raw_points);
points_array = [];
words_size = size(words);

for i = 1:1:(words_size(2))
  str = char(words(i));
  str_size = size(str);

  if str_size(2) > 0
    coordinate = str2double(str);
    points_array = [points_array, coordinate];
  end
end

x = [];
y = [];
z = [];
points_array_size = size(points_array);

labels = []

for i = 1:3:points_array_size(2)
  point = {};
  point.x = points_array(i);
  point.y = points_array(i+1);
  point.z = points_array(i+2);

  labels = [labels, point];
end

save('test.mat', 'labels');
