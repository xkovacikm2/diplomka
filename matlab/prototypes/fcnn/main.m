%%% construct neural network
layers = [
         imageInputLayer([512 512 1])
         convolution2dLayer(3,16,'Stride', 1)
         reluLayer()
         transposedConv2dLayer(3,1,'Stride',2,'Cropping', 1)
         softmaxLayer()
         pixelClassificationLayer()
]

options = trainingOptions('sgdm', ...
                          'Momentum',0.9, ...
                          'InitialLearnRate',1e-3, ...
                          'L2Regularization',0.0005, ...
                          'MaxEpochs',100, ...
                          'MiniBatchSize',4, ...
                          'Shuffle','every-epoch', ...
                          'VerboseFrequency',2);

patient_dir = dir('../../../data/data');
patient_files = patient_dir(3:end);
size_patient_files = size(patient_files);

load(strcat(patient_files(1).folder, '/', patient_files(1).name));
input_images = new_patient.volume;
cor_label = new_patient.labels('cor');
input_labels = reshape(cor_label(1:512, 1:512, :), size(input_images));

for i = 2:1:size_patient_files
  load(strcat(patient_files(i).folder, '/', patient_files(i).name));

  input_image = new_patient.volume;
  input_images = cat(4, input_images, input_image);

  cor_label = new_patient.labels('cor');
  input_label = reshape(cor_label(1:512, 1:512, :), size(input_image));
  input_labels = cat(4, input_labels, input_label);
end

[trainedNet, trainInfo] = trainNetwork(input_images, categorical(input_labels), layers, options);

save('model.mat', 'trainedNet', 'trainInfo');
