%%% Load DICOM dataset and labels
organs = ["oesophagus", "cor", "pulmo_sin", "pulmo_dex", "medulla_spinalis"];
dicom_directories = dir('/home/kovko/diplomka/DOI/train');
label_directories = '/home/kovko/diplomka/labels';

%patient = load_patients(dicom_directories, label_directories, organs);

%%% Transform data
patient_size = size(patient);

for i = 33:1:36
  single_patient = patient(i);
  positions = single_patient.spatial.PatientPositions;

  z_start = positions(1,3);
  z_shift = abs(abs(positions(1,3)) - abs(positions(2,3)));
  size_volume = size(single_patient.volume);
  new_labels = containers.Map;

  for k = 1:1:5
    cor_label = single_patient.labels(char(organs(k)));
    size_cor_label = size(cor_label);
    cor_label_mat = zeros(size_volume(1), size_volume(2), size_volume(4));

    for j = 1:1:size_cor_label(2)
      x = round(cor_label(i).x + size_volume(1)/2);
      y = round(cor_label(i).y + size_volume(2)/2);
      z = round(abs(abs(cor_label(i).z) - abs(z_start))/z_shift);

      cor_label_mat(x,y,z) = 1;
    end

    new_labels(char(organs(k))) = cor_label_mat;
  end

  new_patient.volume = patient(i).volume;
  new_patient.spatial = patient(i).spatial;
  new_patient.dims = patient(i).dims;
  new_patient.labels = new_labels;

  try
    save(strcat('patient_', num2str(i), '.mat'), 'new_patient');
  catch ME
  end
end

