function[ patient ] = load_patients(dicom_directories, label_directories, organs)
  dicom_directories = dicom_directories(3:end); %remove . and ..
  size_organs = size(organs);
  size_dicom_directories = size(dicom_directories);

  for i = 1:1:size_dicom_directories(1)
    patient_dir = dicom_directories(i);
    step_1 = dir(strcat(patient_dir.folder, '/', patient_dir.name));
    step_2 = dir(strcat(step_1(end).folder, '/', step_1(end).name));

    candidate_1 = dir(strcat(step_2(3).folder, '/', step_2(3).name));
    candidate_2 = dir(strcat(step_2(4).folder, '/', step_2(4).name));
    size_candidate_1 = size(candidate_1);
    size_candidate_2 = size(candidate_2);

    if size_candidate_1(1) > size_candidate_2(1)
      slices_dir = candidate_1(3:end);
    else
      slices_dir = candidate_2(3:end);
    end

    [V, spatial, dim] = dicomreadVolume(strcat(slices_dir(1).folder));

    patient_labels = containers.Map;

    for j = 1:1:size_organs(2)
      load(strcat(label_directories, '/', patient_dir.name, '/', organs(j), '.mat'));
      patient_labels(char(organs(j))) = points;
    end

    patient(i).volume = V;
    patient(i).spatial = spatial;
    patient(i).dims = dim;
    patient(i).labels = patient_labels;
  end
end
