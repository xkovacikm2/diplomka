[x, t] = bodyfat_dataset;
net1 = feedforwardnet(10);
net2 = train(net1, x, t, 'useParallel', 'yes', 'showResources', 'yes')
% parallel gpu doesn't work, lacks mac CUDA# Rekurentne neuronove siete
- 2 pouzitia rekurentnych neuronovych sieti
  1. asociativne pamate
  1. vstupno-vystupne mapovacie siete
- klucovy koncept je **stabilita** (zalezi na **spatnej vazbe** modelu)
- stabilita predpoklada koordinaciu medzi prvkami systemu. su 2 pohlady:
  1. **inziniersky:** ohraniceny vstup - ohraniceny vystup (BIBO) kriterium
  1. **nelinearne dynamicke systemy:** - Lyapunovov

## Dynamicke systemy
- stavovo-priestorovy model, poziva stavove premenne, ktore sa v case menia
- stav x(t) = $[x_1(t), x_2(t), ...,x_n(t)]^T$ je rad systemu
- bezi v spojitom case: $\frac{dx(t)}{dt} = F(x(t))$
- F je funkcny vektor, kazdy komponent je nelinearna funkcia, ktorej argumenty su akekolvek elementy x
- **System unfolding** - trajektoria v stavovom priestore
- **State portrait** - vsetky trajektorie su superimposed

## Neurodynamicke modely
- systemy zaujmu maju charakteristiky:
  - vela stupnov volnosti - vypoctova sila a odolnost voci chybam takehoto systemu je vysledkom kolektivnej dynamiky systemu
  - nelinearita
  - dissipation - charakterizovana konvergenciou stavoveho priestoru do menejdimenzii po istom case
  - sum - intrinsna charakteristika

## Hopfieldov model
- plne prepojena vrstva n neuronov
- neuron: 2 stavy
- konfiguracia: S = [$S_1, S_2, ..., S_n$]
- vahy: $w_ij$ \tildeascii $j \rightarrow i$, ak $w_ij$ > 0 potom excitacny
- postsynapticky potencial: vnutorne magneticke pole
- prah excitacie neuronu: externe pole
- efektivny postsynapticky potencial
- update stavu neuronu
  - synchronne(paralelne prepinanie stavov): nove aktivity neuronu hladam podla povodnych hodnot, jednym relaxacnym cyklom updatnem vsetky neurony
  - asynchronne(sekvencne): vyberiem si nahodne jeden neuron a ten preklopim, potom si vyber nahodne novy a preklopim ho s pouzitim noveho stavu pred nim preklopeneho neuronu
  - evolucna konfiguracia: $S(0) \rightarrow S(1) \rightarrow$ (relaxacny proces)
      - dynamika synchronizacie: trajektoria po hranach hyperkocky
- **konfiguracia energie** - $E(S) = - \frac{1}{2} \sum_i \sum_j w_{ij}S_i S_j$

### Asymptoticke spravanie
- nahodne nainicializovana konfiguracia = chaoticke spravanie kde E klesa a stupa. Vznika ked matica vah nieje symetricka
