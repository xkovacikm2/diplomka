# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kovko/Documents/FIIT/ING/diplomovka/cpp_training/load_dicom/main.cpp" "/home/kovko/Documents/FIIT/ING/diplomovka/cpp_training/load_dicom/cmake-build-debug/CMakeFiles/load_dicom.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ITK_IO_FACTORY_REGISTER_MANAGER"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "ITKFactoryRegistration"
  "/usr/local/include/ITK-4.13"
  "/usr/local/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
