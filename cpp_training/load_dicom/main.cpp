#include <itkGDCMSeriesFileNames.h>
#include <itkImageSeriesReader.h>
#include <itkGDCMImageIO.h>
#include <itkImageFileWriter.h>

using PixelType = signed short;
constexpr unsigned int Dimension = 3;
using ImageType = itk::Image< PixelType, Dimension >;

int main() {
    std::string dirName("/home/kovko/Documents/FIIT/ING/diplomovka/DOI/LCTSC-Train-S1-002/1.3.6.1.4.1.14519.5.2.1.7014.4598.665008031803939868532289200439/1.3.6.1.4.1.14519.5.2.1.7014.4598.291449913947522380633360215042");
    auto nameGenerator = itk::GDCMSeriesFileNames::New();

    nameGenerator->SetUseSeriesDetails(true);
    nameGenerator->SetGlobalWarningDisplay(false);
    nameGenerator->SetDirectory(dirName);

    auto seriesUID = nameGenerator->GetSeriesUIDs();

    for (auto UID : seriesUID) {
        std::cout << UID.c_str() << std::endl;

        auto fileNames = nameGenerator->GetFileNames(UID.c_str());

        auto reader = itk::ImageSeriesReader<ImageType>::New();
        auto dicomIO = itk::GDCMImageIO::New();
        reader->SetImageIO(dicomIO);
        reader->SetFileNames(fileNames);

        auto writer = itk::ImageFileWriter< ImageType >::New();
        writer->SetFileName("/home/kovko/Downloads/test.nrrd");
        writer->SetInput(reader->GetOutput());
        writer->Update();
    }

    return 0;
}