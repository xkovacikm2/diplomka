import numpy as np
import pydicom as dicom
import scipy.ndimage
import os
from scipy.io import loadmat
from shapely import geometry

def load_labels(path, first_slice):
    raw = loadmat(path)['points'][0]
    points = np.zeros((420,1)).tolist()

    for idx, _ in enumerate(points):
        points[idx] = []

    for point in raw:
        points[int(np.abs(point[2][0][0]) - np.abs(first_slice.SliceLocation))].append([(point[0][0][0] + 512/2)*500/512, point[1][0][0]*500/512])

    return [None if not poly else geometry.Polygon(poly) for poly in points]

def load_scan(path):
    slices = [dicom.read_file(path + '/' + s) for s in os.listdir(path)]
    slices.sort(key=lambda x: int(x.InstanceNumber))
    try:
        slice_thickness = np.abs(slices[0].ImagePositionPatient[2] - slices[1].ImagePositionPatient[2])
    except:
        slice_thickness = np.abs(slices[0].SliceLocation - slices[1].SliceLocation)

    for s in slices:
        s.SliceThickness = slice_thickness

    return slices

def dice(p1, p2):
    try:
        i = p1.intersection(p2)
        u = p1.union(p2)
        return 2*i.area/u.area
    except:
        return None


def get_pixels_hu(scans):
    image = np.stack([s.pixel_array for s in scans])
    image = image.astype(np.int16)

    # Set outside-of-scan pixels to 1
    # The intercept is usually -1024, so air is approximately 0
    image[image == -2000] = 0

    # Convert to Hounsfield units (HU)
    intercept = scans[0].RescaleIntercept
    slope = scans[0].RescaleSlope

    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int16)

    image += np.int16(intercept)

    return np.array(image, dtype=np.int16)


def resample(image, scan, new_spacing=[1, 1, 1]):
    # Determine current pixel spacing
    spacing = map(float, ([scan[0].SliceThickness] + [scan[0].PixelSpacing[0]] + [scan[0].PixelSpacing[1]]))
    spacing = np.array(list(spacing))

    resize_factor = spacing / new_spacing
    new_real_shape = image.shape * resize_factor
    new_shape = np.round(new_real_shape)
    real_resize_factor = new_shape / image.shape
    new_spacing = spacing / real_resize_factor

    image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)

    return image, new_spacing
