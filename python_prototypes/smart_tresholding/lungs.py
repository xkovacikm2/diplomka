import numpy as np
from skimage import morphology
from skimage import measure
from sklearn.cluster import KMeans
from skimage.segmentation import active_contour
from shapely import geometry
from PIL import Image, ImageDraw


def normalize(img):
    mean = np.mean(img)
    std = np.std(img)
    img = img - mean
    img = img / std
    return img


def repair_overflow_with_mean(img):
    row_size = img.shape[0]
    col_size = img.shape[1]

    middle = img[int(col_size / 5):int(col_size / 5 * 4), int(row_size / 5):int(row_size / 5 * 4)]
    mean = np.mean(middle)
    max = np.max(img)
    min = np.min(img)
    img[img == max] = mean
    img[img == min] = mean

    return img, middle


def find_treshold(img):
    kmeans = KMeans(n_clusters=2).fit(np.reshape(img, [np.prod(img.shape), 1]))
    centers = sorted(kmeans.cluster_centers_.flatten())
    return np.mean(centers)


def make_lungmask(img):
    row_size = img.shape[0]
    col_size = img.shape[1]

    img, middle = repair_overflow_with_mean(normalize(img))

    threshold = find_treshold(middle)
    thresh_img = np.where(img < threshold, 1.0, 0.0)

    eroded = morphology.erosion(thresh_img, np.ones([3, 3]))
    dilation = morphology.dilation(eroded, np.ones([3, 3]))

    labels = measure.label(dilation)
    regions = measure.regionprops(labels)
    good_labels = []

    for prop in regions:
        B = prop.bbox
        if B[2] - B[0] < row_size / 10 * 9 and B[3] - B[1] < col_size / 10 * 9 and B[0] > row_size / 5 and B[
            2] < col_size / 5 * 4:
            good_labels.append(prop.label)
    mask = np.ndarray([row_size, col_size], dtype=np.int8)
    mask[:] = 0

    for N in good_labels:
        mask = mask + np.where(labels == N, 1, 0)

    mask = morphology.dilation(mask, np.ones([10, 10]))
    mask = morphology.erosion(mask, np.ones([10, 10]))

    return mask


def init_snake_with_mask(mask):
    left_xs = []
    right_xs = []
    middle_left_xs = []
    middle_right_xs = []
    middle_left_ys = []
    middle_right_ys = []
    left_ys = []
    right_ys = []

    for y, line in enumerate(mask):
        line_iterator = 0
        tmpx = 0
        tmpy = 0

        for x, value in enumerate(line):

            if value == 1 and mask[y, x-1] == 0:

                if line_iterator == 0:
                    left_xs.append(x)
                    left_ys.append(y)
                    line_iterator = 1
                else:
                    middle_right_xs.append(x)
                    middle_right_ys.append(y)
                    line_iterator = 2

            elif value == 1 and mask[y, x+1] == 0:

                if line_iterator == 1:
                    tmpx = x
                    tmpy = y
                else:
                    middle_left_xs.append(tmpx)
                    middle_left_ys.append(tmpy)
                    tmpx = x
                    tmpy = y

        if tmpx + tmpy > 0:
            right_xs.append(tmpx)
            right_ys.append(tmpy)

    left = reject_outliers_from_contours(np.array([left_xs, left_ys]).T)
    middle_left = reject_outliers_from_contours(np.array([middle_left_xs, middle_left_ys]).T)
    middle_right = reject_outliers_from_contours(np.array([middle_right_xs, middle_right_ys]).T)
    right = reject_outliers_from_contours(np.array([right_xs, right_ys]).T)

    join_left = np.concatenate((left, np.flip(middle_left, axis = 0)))
    join_right = np.concatenate((middle_right, np.flip(right, axis = 0)))

    return join_left, join_right


def reject_outliers_from_contours(data, m = 2, axis = 0):
    return data[abs(data[:, axis]) - np.median(data[:, axis]) < m * np.std(data[:, axis])]


def apply_snake(imgs_after_resamp, init_snake):
    left_lung = [tuned_single_layer_snake(slice, init_snake[i][0]) for i, slice in enumerate(imgs_after_resamp)]
    right_lung = [tuned_single_layer_snake(slice, init_snake[i][1]) for i, slice in enumerate(imgs_after_resamp)]

    return left_lung, right_lung


def tuned_single_layer_snake(slice, init):
    return active_contour(slice, init, beta = 0.5, w_line = -0.25, w_edge = 1.4)


# this didnt work as intended
def lungmask_from_snake(shape, snake_result):
    mask = np.zeros(shape)

    for idx, snake_slice in enumerate(snake_result[0]):
        for point in snake_slice:
            mask[idx, int(point[0]), int(point[1])] = 1

    for idx, snake_slice in enumerate(snake_result[1]):
        for point in snake_slice:
            mask[idx, int(point[0]), int(point[1])] = 1

        mask[idx] = morphology.dilation(mask[idx], np.ones([10, 10]))
        mask[idx] = morphology.erosion(mask[idx], np.ones([10, 10]))

    return mask


def snake_to_polygon(snake):
    polys_left  = [geometry.Polygon(snake_slice) for snake_slice in snake[0]]
    polys_right = [geometry.Polygon(snake_slice) for snake_slice in snake[1]]

    return polys_left, polys_right


def apply_poly_as_mask(img, polygons):
    masked_image = []

    for idx, slice in enumerate(img):
        masked_slice = []
        print("Slice number: %d" % idx)
        for y, row in enumerate(slice):
            masked_row = []
            for x, value in enumerate(row):
                p = geometry.Point(x,y)
                if polygons[0][idx].contains(p) or polygons[1][idx].contains(p):
                    masked_row.append(value)
                else:
                    masked_row.append(0)
            masked_slice.append(masked_row)
        masked_image.append(masked_slice)

    return np.array(masked_image)


def mask_from_snake(img_shape, snake):
    img = Image.new('L', (img_shape[0], img_shape[1]), 0)
    ImageDraw.Draw(img).polygon(snake.flatten(), outline=1, fill=1)
    mask = np.array(img)
    return mask