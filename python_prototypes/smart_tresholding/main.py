import numpy as np
from glob import glob
import nrrd
import utils
import lungs

# desktop
data_path = "/home/kovko/Documents/prace/STU/diplomovka/DOI/LCTSC-Train-S1-001/1.3.6.1.4.1.14519.5.2.1.7014.4598.117430069853376188015337939664/1.3.6.1.4.1.14519.5.2.1.7014.4598.330486033168582850130357263530"
# mac
# data_path = "/home/kovko/Documents/FIIT/ING/diplomovka/DOI/Train/LCTSC-Train-S1-001/1.3.6.1.4.1.14519.5.2.1.7014.4598.117430069853376188015337939664/1.3.6.1.4.1.14519.5.2.1.7014.4598.330486033168582850130357263530"
# titan
# data_path = "/home/kovko/diplomka/DOI/train/LCTSC-Train-S1-001/1.3.6.1.4.1.14519.5.2.1.7014.4598.117430069853376188015337939664/1.3.6.1.4.1.14519.5.2.1.7014.4598.330486033168582850130357263530"
g = glob(data_path + '/*.dcm')

patient = utils.load_scan(data_path)
imgs_to_process = utils.get_pixels_hu(patient)

imgs_after_resamp, _ = utils.resample(imgs_to_process, patient, [1,1,1])

### create mask and serialize it
lung_mask = np.array([lungs.make_lungmask(img) for img in imgs_after_resamp])
# masked_lung = lung_mask*imgs_after_resamp
# nrrd.write("lung_tresholding.nrrd", masked_lung)

# use snake initialized with treshold
init_snake = [lungs.init_snake_with_mask(lung_slice) for lung_slice in lung_mask]
snake_result = lungs.apply_snake(imgs_after_resamp, init_snake)

### left_lung_mask = np.array([lungs.mask_from_snake(imgs_after_resamp[idx].shape, snake_result[0][idx]) for idx, _ in enumerate(snake_result[0])])
# right_lung_mask = np.array([lungs.mask_from_snake(imgs_after_resamp[idx].shape, snake_result[1][idx]) for idx, _ in enumerate(snake_result[1])])
#
# masked_lung = ((left_lung_mask + right_lung_mask) * imgs_after_resamp)
# nrrd.write("lung_snake.nrrd", masked_lung)

polygons = lungs.snake_to_polygon(snake_result)
# masked_lung = lungs.apply_poly_as_mask(imgs_after_resamp, polygons)

### load ground truth
right_lung_gt = utils.load_labels('/home/kovko/Documents/prace/STU/diplomovka/labels/LCTSC-Train-S1-001/cor.mat', patient[139])

for idx, p in enumerate(right_lung_gt):
    if p != None:
        print(utils.dice(p, polygons[1][idx]))