import numpy as np
from glob import glob
import utils
import skimage.future.graph as graph_algs
from skimage import graph, data, io, segmentation, color
from skimage.measure import regionprops
import matplotlib.pyplot as plt

data_path = "data/patient-s03-12"

patient = utils.load_scan(data_path)
imgs_to_process = utils.get_pixels_hu(patient)

imgs_after_resamp, _ = utils.resample(imgs_to_process, patient, [1,1,1])

slice = imgs_after_resamp[120]

plt.imshow(slice)
plt.show()

labels = segmentation.slic(slice, compactness=0.1, multichannel=False, n_segments=500)
labels = labels + 1
regions = regionprops(labels)
label_rgb = color.label2rgb(labels, slice, kind='avg')

plt.imshow(label_rgb)
plt.show()

rag = graph_algs.rag_mean_color(slice, labels)

for region in regions:
    rag.node[region['label']]['centroid'] = region['centroid']

edges_drawn = utils.display_edges(slice, rag, np.inf)

plt.imshow(edges_drawn)
plt.show()

for i in [0.1, 0.3, 0.5]:
    result = graph_algs.cut_normalized(labels, rag, thresh=i, num_cuts=5)
    result_rgb = color.label2rgb(result, slice, kind='avg')

    plt.imshow(result_rgb)
    plt.show()
