import tensorflow as tf
import pdb
from hyper_params import *

class FullyConnected():
    def __init__(self, in_depth=INPUT_DEPTH, out_depth=OUTPUT_DEPTH,
                 in_size=INPUT_SIZE, out_size=OUTPUT_SIZE, num_classes=OUTPUT_CLASSES,
                 learning_rate=LEARNING_RATE, in_classes=INPUT_CLASSES):

        self.base_init = tf.truncated_normal_initializer(stddev=0.5)  # Initialise weights
        self.reg_init = tf.contrib.layers.l2_regularizer(scale=0.2)  # Initialise regularisation (was useful)

        with tf.variable_scope('FullyConnected'):
            # Define placeholders
            self.training = tf.placeholder(tf.bool)
            self.model_input = tf.placeholder(tf.float32, shape=(in_size, in_size, num_classes))
            self.model_topology = tf.placeholder(tf.float32, shape=(in_size, in_size, 3))
            self.model_labels = tf.placeholder(tf.int32, shape=(out_size, out_size, 1))

            # Merge with coordinates
            merged_features = tf.concat([self.model_input, self.model_topology], -1)

            # Fully connected layer
            flattened_features = tf.reshape(merged_features, [-1, num_classes + 3])
            flat_mask = tf.contrib.layers.fully_connected(flattened_features, 1, activation_fn=tf.sigmoid)

            # Calculate error for each voxel 
            flat_labels = tf.reshape(self.model_labels, [-1, 1]) * 10
            self.loss = tf.losses.mean_squared_error(flat_labels, flat_mask) / 10

            # Set predictions
            self.predictions = tf.reshape(tf.cast(tf.round(flat_mask), tf.int32), [out_size, out_size, 1])

            self.trainer = tf.train.AdamOptimizer(learning_rate=learning_rate, name='FullyConnectedAdam')
            self.extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS) 
            with tf.control_dependencies(self.extra_update_ops):
                self.train_op = self.trainer.minimize(self.loss)
