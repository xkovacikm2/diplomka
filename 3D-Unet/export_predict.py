from unet import *
import hickle
import matplotlib
import sys
import pdb
from export_hyper_params import *
from data_processing import *
from visualisations import *

test_predictions = hickle.load(PICKLES_TEST)
test_predictions = np.expand_dims(test_predictions, axis=5)
test_model_path = sys.argv[1]
organ = sys.argv[2]
iteration = sys.argv[3]
test_model_name = f"{MODEL_NAME}_{organ}-{iteration}"

config = tf.ConfigProto()
unet = UNetwork(drop=UNET_DROPOUT, base_filt=UNET_BASE_FILTER, should_pad=UNET_SHOULD_PADDING)  # MODEL DEFINITION

with tf.Session(config=config) as sess:
    print('Loading saved model ...')
    restore_path = f"{test_model_path}/{test_model_name}.meta"
    print(restore_path)
    # restorer = tf.train.import_meta_graph(restore_path)
    saver = tf.train.Saver()
    saver.restore(sess, f"{test_model_path}/{test_model_name}")
    # restorer.restore(sess, f"{SAVE_PATH}/{test_model_name}.ckpt")
    # sess.run(tf.global_variables_initializer())
    print("Model sucessfully restored")

    i = 0

    while i < len(test_predictions):
        predictions = []
        test_dict = {
            unet.training: False,
            unet.model_input: [test_predictions[i]]
        }

        session_result = sess.run([unet.predictions], feed_dict=test_dict)
        preds = [np.squeeze(session_result)]
        predictions.append(preds)

        reconstructed_predictions = reconstruct_from_patches(np.squeeze(predictions))
        # reconstructed_ys = reconstruct_from_patches(np.squeeze(y_s))
        reconstructed_image = reconstruct_from_patches(np.squeeze(test_predictions[i]))

        print_to_nrrd(reconstructed_predictions, f"{PREDICTION_RESULTS_PATH}/nrrd/{i}.nrrd")
        # print_to_nrrd(reconstructed_ys, f"{PREDICTION_RESULTS_PATH}/nrrd/{i}_labels.nrrd")
        print_to_nrrd(reconstructed_image, f"{PREDICTION_RESULTS_PATH}/nrrd/{i}_orig.nrrd")

        i += 1
