import numpy as np
import tensorflow as tf
import tensorlayer as tl
from hyper_params2 import *
import pdb

class UNetwork2():

    def conv_batch_relu(self, tensor, filters, kernel=[3, 3, 3], stride=[1, 1, 1], is_training=True):
        padding = 'valid'
        if self.should_pad: padding = 'same'

        conv = tf.layers.conv3d(tensor, filters, kernel_size=kernel, strides=stride, padding=padding,
                                kernel_initializer=self.base_init, kernel_regularizer=self.reg_init)
        conv = tf.layers.batch_normalization(conv, training=is_training)
        conv = tf.nn.relu(conv)
        return conv

    def upconvolve(self, tensor, filters, kernel=2, stride=2, scale=4, activation=None):
        padding = 'valid'
        if self.should_pad: padding = 'same'
        conv = tf.layers.conv3d_transpose(tensor, filters, kernel_size=kernel, strides=stride, padding=padding,
                                          use_bias=False,
                                          kernel_initializer=self.base_init, kernel_regularizer=self.reg_init)
        return conv

    def centre_crop_and_concat(self, prev_conv, up_conv):
        p_c_s = prev_conv.get_shape()
        u_c_s = up_conv.get_shape()
        offsets = np.array([0, (p_c_s[1] - u_c_s[1]) // 2, (p_c_s[2] - u_c_s[2]) // 2,
                            (p_c_s[3] - u_c_s[3]) // 2, 0], dtype=np.int32)
        size = np.array([-1, u_c_s[1], u_c_s[2], u_c_s[3], p_c_s[4]], np.int32)
        prev_conv_crop = tf.slice(prev_conv, offsets, size)
        up_concat = tf.concat((prev_conv_crop, up_conv), 4)
        return up_concat

    def __init__(self, base_filt=8, in_depth=INPUT_DEPTH, out_depth=OUTPUT_DEPTH,
                 in_size=INPUT_SIZE, out_size=OUTPUT_SIZE, num_classes=OUTPUT_CLASSES,
                 learning_rate=LEARNING_RATE, print_shapes=True,
                 drop=UNET_DROPOUT, should_pad=False):

        self.base_init = tf.truncated_normal_initializer(stddev=0.1)  # Initialise weights
        self.reg_init = tf.contrib.layers.l2_regularizer(scale=0.1)  # Initialise regularisation (was useful)

        self.should_pad = should_pad  # To pad or not to pad, that is the question
        self.drop = drop  # Set dropout rate

        with tf.variable_scope('3DuNet2'):
            self.training = tf.placeholder(tf.bool)
            self.do_print = print_shapes
            # Define placeholders for feed_dict
            self.model_input = tf.placeholder(tf.float32, shape=(None, in_depth, in_size, in_size, 2))
            self.model_labels = tf.placeholder(tf.int32, shape=(None, out_depth, out_size, out_size, 1))

            labels_one_hot = tf.squeeze(tf.one_hot(self.model_labels, num_classes, axis=-1), axis=-2)

            if self.do_print:
                print('Input features shape', self.model_input.get_shape())
                print('Labels shape', self.model_labels.get_shape())

            # Level zero
            conv_0_1 = self.conv_batch_relu(self.model_input, base_filt, is_training=self.training)
            conv_0_2 = self.conv_batch_relu(conv_0_1, base_filt * 2, is_training=self.training)

            # Level one
            max_1_1 = tf.layers.max_pooling3d(conv_0_2, UNET_MAXPOOLING_STRIDE, UNET_MAXPOOLING_STRIDE)  # Stride, Kernel
            conv_1_1 = self.conv_batch_relu(max_1_1, base_filt * 2, is_training=self.training)
            conv_1_2 = self.conv_batch_relu(conv_1_1, base_filt * 4, is_training=self.training)
            dropout_1_2 = tf.layers.dropout(conv_1_2, rate=self.drop, training=self.training)

            # Level two
            max_2_1 = tf.layers.max_pooling3d(dropout_1_2, UNET_MAXPOOLING_STRIDE, UNET_MAXPOOLING_STRIDE)  # Stride, Kernel
            conv_2_1 = self.conv_batch_relu(max_2_1, base_filt * 4, is_training=self.training)
            conv_2_2 = self.conv_batch_relu(conv_2_1, base_filt * 8, is_training=self.training)
            dropout_2_2 = tf.layers.dropout(conv_2_2, rate=self.drop, training=self.training)

            # Level three
            max_3_1 = tf.layers.max_pooling3d(dropout_2_2, UNET_MAXPOOLING_STRIDE, UNET_MAXPOOLING_STRIDE)  # Stride, Kernel
            conv_3_1 = self.conv_batch_relu(max_3_1, base_filt * 8, is_training=self.training)
            conv_3_2 = self.conv_batch_relu(conv_3_1, base_filt * 16, is_training=self.training)
            dropout_3_2 = tf.layers.dropout(conv_3_2, rate=self.drop, training=self.training)

            # Level two
            up_conv_3_2 = self.upconvolve(dropout_3_2, base_filt * 16, kernel=UNET_MAXPOOLING_STRIDE, stride=UNET_MAXPOOLING_STRIDE)
            concat_2_1 = self.centre_crop_and_concat(dropout_2_2, up_conv_3_2)
            conv_2_3 = self.conv_batch_relu(concat_2_1, base_filt * 8, is_training=self.training)
            conv_2_4 = self.conv_batch_relu(conv_2_3, base_filt * 8, is_training=self.training)
            dropout_2_4 = tf.layers.dropout(conv_2_4, rate=self.drop, training=self.training)

            # Level one
            up_conv_2_1 = self.upconvolve(dropout_2_4, base_filt * 8, kernel=UNET_MAXPOOLING_STRIDE, stride=UNET_MAXPOOLING_STRIDE)
            concat_1_1 = self.centre_crop_and_concat(dropout_1_2, up_conv_2_1)
            conv_1_3 = self.conv_batch_relu(concat_1_1, base_filt * 4, is_training=self.training)
            conv_1_4 = self.conv_batch_relu(conv_1_3, base_filt * 4, is_training=self.training)
            dropout_1_4 = tf.layers.dropout(conv_1_4, rate=self.drop, training=self.training)

            # Level zero
            up_conv_1_0 = self.upconvolve(dropout_1_4, base_filt * 4, UNET_MAXPOOLING_STRIDE, UNET_MAXPOOLING_STRIDE)
            concat_0_1 = self.centre_crop_and_concat(conv_0_2, up_conv_1_0)
            conv_0_3 = self.conv_batch_relu(concat_0_1, base_filt * 2, is_training=self.training)
            conv_0_4 = self.conv_batch_relu(conv_0_3, base_filt * 2, is_training=self.training)
            dropout_0_4 = tf.layers.dropout(conv_0_4, rate=self.drop, training=self.training)
            conv_out = tf.layers.conv3d(dropout_0_4, OUTPUT_CLASSES, [1,1,1], [1,1,1], padding='same')

            self.predictions = tf.expand_dims(tf.argmax(conv_out, axis=-1, output_type=tf.int32), -1)

            if self.do_print:
                print('Model Convolution output shape', conv_out.get_shape())
                print('Model Argmax output shape', self.predictions.get_shape())

            loss_predictions = tf.nn.softmax(conv_out)
            self.loss = 1 - tl.cost.dice_coe(loss_predictions, labels_one_hot, loss_type='sorensen')

            self.trainer = tf.train.AdamOptimizer(learning_rate=learning_rate, beta1=0.8, beta2=0.9, name='UnetAdam')

            self.extra_update_ops = tf.get_collection(
                tf.GraphKeys.UPDATE_OPS)  # Ensure correct ordering for batch-norm to work
            with tf.control_dependencies(self.extra_update_ops):
                self.train_op = self.trainer.minimize(self.loss)

