import os
import hickle
import nrrd
import numpy as np
import pydicom as dicom
import scipy.ndimage
import scipy.spatial
import multiprocessing
import pdb
import sys
from data_processing import *
from resampling_helpers import *
from hyper_params2 import *


def get_bbox(image):
    hit_layers = np.nonzero(image)
    bbox = []

    for layer in hit_layers:
        bbox.append(slice(min(layer), max(layer)))

    return bbox


def zoom_bbox(bbox, max_shape, factor=0.1):
    new_bbox = []

    for i in range(len(bbox)):
        start = bbox[i].start
        start = int(start * (1 - factor)) 

        if start < 0:
            start = 0

        stop = bbox[i].stop
        stop = int(stop * (1 + factor))

        if stop >= max_shape[i]:
            stop = max_shape[i] - 1

        new_bbox.append(slice(start, stop))
    return new_bbox



def resample_scan(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)
    return image


def resample_label(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.zoom(image, real_resize_factor, prefilter=False, mode='nearest', order=0)
    return image


def get_positions_and_spacing(patient):
    top_left_coordinate = [float(i) for i in patient[0].ImagePositionPatient]

    density = [
        float(patient[0].PixelSpacing[0])*patient[0].Rows, float(patient[1].PixelSpacing[1])*patient[1].Columns,
        abs(abs(int(patient[0].SliceLocation)) - abs(int(patient[-1].SliceLocation)))
    ]

    return top_left_coordinate, density


def label_distance_transform(labels):
    m_c_labels = [labels]
    classes = np.unique(labels).tolist()
    classes.remove(0) #ignore background

    for class_num in classes:
        inv_labels = np.ones(labels.shape).astype(int)
        inv_labels[labels==class_num] = 0
        distances = edt.edt(inv_labels, black_border=False)
        m_c_labels.append(distances)
        
    return m_c_labels


def get_dataset(data_dir, anns_dir, segm_dir):
    # Match DCM volumes with corresponding annotation files
    data_out = []
    resample_shape = np.array([RESCALE_DEPTH, RESCALE_SIZE, RESCALE_SIZE])
    dict_dcm = return_dcm(data_dir)
    dict_nrrd = return_nrrd(anns_dir)
    d_segm = list(return_nrrd(segm_dir).values())
    keys = list(dict_nrrd.keys())
    d_dcm = []
    d_nrrd = []

    for key in keys:
        d_dcm.append(dict_dcm[key])
        d_nrrd.append(dict_nrrd[key])
        
    for i in range(len(d_nrrd)):
        print(f"Preparing: {i}")

        gt_seg, _ = nrrd.read(d_nrrd[i])
        gt_seg = gt_seg.astype(int)
        gt_seg[gt_seg != organ_encoding()[sys.argv[1]]] = 0
        gt_seg[gt_seg == organ_encoding()[sys.argv[1]]] = 1
        my_seg, _ = nrrd.read(d_segm[i])
        my_seg = my_seg.astype(int)
        patient = load_scan(d_dcm[i])
        voxels = get_pixels_hu(patient)

        # Rotate and flip annotations to match volumes
        gt_seg = scipy.ndimage.interpolation.rotate(gt_seg, 90, axes=(2, 0), order=0)

        # Get BBox and zoom it a little to keep some varying space
        mask = get_bbox(my_seg)
        mask = zoom_bbox(mask, my_seg.shape, 0.1)

        # Crop all data using bbox
        cropped_gt = gt_seg[mask]
        cropped_my = my_seg[mask]
        cropped_vox = voxels[mask]

        # Rescale to hyper_params2 defined shape 
        resampled_voxels = resample_scan(cropped_vox, resample_shape)
        resampled_labels = resample_label(cropped_gt, resample_shape)
        resampled_my_seg = resample_label(cropped_my, resample_shape)

        data_out.append((np.concatenate((np.expand_dims(resampled_voxels, axis=3), np.expand_dims(resampled_my_seg, axis=3)), axis=3), resampled_labels))
    return data_out


def do_dumping(iteration):
    pickle_names = [PICKLES_TRAIN1, PICKLES_TRAIN2, PICKLES_TRAIN3, PICKLES_TRAIN4, PICKLES_TRAIN5, PICKLES_TEST]
    data_train_dirs = [data_train_dir1, data_train_dir2, data_train_dir3, data_train_dir4, data_train_dir5, data_test_dir]
    anns_train_dirs = [anns_train_dir1, anns_train_dir2, anns_train_dir3, anns_train_dir4, anns_train_dir5, anns_test_dir]
    segm_train_dirs = [segm_train_dir1, segm_train_dir2, segm_train_dir3, segm_train_dir4, segm_train_dir5, segm_test_dir]
    # pickle_names = [PICKLES_TEST]
    # data_train_dirs = [data_test_dir]
    # anns_train_dirs = [anns_test_dir]
    # segm_train_dirs = [segm_test_dir]

    pickle_name = pickle_names[iteration]
    data_dir = data_train_dirs[iteration]
    anns_dir = anns_train_dirs[iteration]
    segm_dir = segm_train_dirs[iteration]

    print("Fetching ", pickle_name)
    train = get_dataset(data_dir, anns_dir, segm_dir)

    print("Dumping ", pickle_name)
    hickle.dump(train, pickle_name, mode='w')
    print("Dumped ", pickle_name)

pool = multiprocessing.Pool(3)
pool.map(do_dumping, range(0, 6))

# for i in range(0,1):
#    do_dumping(i)
