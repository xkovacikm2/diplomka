# 1. nacitat dicom, moje nrrd, gt nrrd
# for each slice in slices
# 2. extrahovat kontury z mojho nrrd
# 3. inicializovat snakea z kontury
# 4. vyhodnotit iou 

from resampling_helpers import *
import numpy as np
import cv2
import sys
import pdb
import scipy.ndimage
import nrrd
import skimage.segmentation
import skimage.measure
import skimage.morphology
import scipy.ndimage
from matplotlib import pyplot as plt

segmentation_folder = sys.argv[1]
dicom_folder = sys.argv[2]
labels_folder = sys.argv[3]

d_seg = list(return_nrrd(segmentation_folder).values())
d_dcm = list(return_dcm(dicom_folder).values())
d_lab = list(return_nrrd(labels_folder).values())
diamond = skimage.morphology.diamond(10)

for i in range(len(d_seg)):
    seg = nrrd.read(d_seg[i])[0].astype(int)
    label = nrrd.read(d_lab[i])[0].astype(int)
    patient = load_scan(d_dcm[i])

    voxels = get_pixels_hu(patient)
    label = scipy.ndimage.interpolation.rotate(label, 90, axes=(2,0), order=0)
    
    result = np.zeros(label.shape)

    for j in range(seg.shape[0]):
        seg_slice = seg[j]

        if np.count_nonzero(seg_slice) == 0:
            continue
        
        dilated_slice = skimage.morphology.binary_dilation(seg_slice, diamond)
        dilated_slice[dilated_slice] = 1
        dilated_slice[dilated_slice == False] = 0
        contours = skimage.measure.find_contours(dilated_slice, 0)
        contour = contours[0]
        
        # http://scikit-image.org/docs/dev/api/skimage.segmentation.html#skimage.segmentation.active_contour
        snake = skimage.segmentation.active_contour(voxels[j], contour, alpha=0.01, beta=0.01, w_line=-0.5, w_edge=2, bc='free')
        snake = np.unique(np.round(snake).astype(int), axis=0)

        fig = plt.figure()
        ax1 = fig.add_subplot(111)
        ax1.imshow(voxels[j], cmap=plt.cm.gray)
        ax1.plot(contour[:,1], contour[:,0], '-r', lw=1)
        ax1.plot(snake[:,1], snake[:,0], '.b', lw=0.3)
        ax1.axis([0, 512, 512, 0])
        plt.savefig(f"snake/hearth_{i}_{j}.png")

        mask = np.zeros(voxels[j].shape)
        # Draw contour
        mask[snake[:,0], snake[:,1]] = 1
        # Fill hole
        mask = scipy.ndimage.binary_fill_holes(mask)
        pdb.set_trace()
