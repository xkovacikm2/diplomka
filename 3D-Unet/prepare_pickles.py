import os
import hickle
import nrrd
import edt
import numpy as np
import pydicom as dicom
import scipy.ndimage
import scipy.spatial
import multiprocessing
import pdb
from hyper_params import *
from data_processing import *
from resampling_helpers import *


def resample_scan(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)
    return image


def resample_label(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.zoom(image, real_resize_factor, prefilter=False, mode='nearest', order=0)
    return image


def get_positions_and_spacing(patient):
    top_left_coordinate = [float(i) for i in patient[0].ImagePositionPatient]

    density = [
        float(patient[0].PixelSpacing[0])*patient[0].Rows, float(patient[1].PixelSpacing[1])*patient[1].Columns,
        abs(abs(int(patient[0].SliceLocation)) - abs(int(patient[-1].SliceLocation)))
    ]

    return top_left_coordinate, density


def add_scaled_positions(resampled_voxels, resampled_labels, top_left, density):
    shape = resampled_voxels.shape
    scaled_density = np.array(density)/np.array(list(shape))
    augmented_voxels = np.zeros((shape[0],shape[1], shape[2], 4))
    augmented_labels = np.zeros((shape[0],shape[1], shape[2], 1))

    for x in range(shape[0]):
        for y in range(shape[1]):
            for z in range(shape[2]):
                augmented_labels[x, y, z, 0] = resampled_labels[x, y, z]

                augmented_voxels[x, y, z, 0] = resampled_voxels[x, y, z]
                augmented_voxels[x, y, z, 1] = top_left[0] + scaled_density[0]*x
                augmented_voxels[x, y, z, 2] = top_left[1] + scaled_density[1]*y
                augmented_voxels[x, y, z, 3] = top_left[2] + scaled_density[2]*z

    return augmented_voxels, augmented_labels


def label_distance_transform(labels):
    m_c_labels = [labels]
    classes = np.unique(labels).tolist()
    classes.remove(0) #ignore background

    for class_num in classes:
        inv_labels = np.ones(labels.shape).astype(int)
        inv_labels[labels==class_num] = 0
        distances = edt.edt(inv_labels, black_border=False)
        m_c_labels.append(distances)
        
    return m_c_labels


def get_dataset(data_dir, anns_dir):
    # Match DCM volumes with corresponding annotation files
    data_out = []
    resample_shape = np.array([RESCALE_DEPTH, RESCALE_SIZE, RESCALE_SIZE])
    d_dcm = return_dcm(data_dir)
    d_nrrd = return_nrrd(anns_dir)
    for i in d_nrrd:
        print("Preparing: " + i)

        seg, opts = nrrd.read(d_nrrd[i])
        seg = seg.astype(int)
        patient = load_scan(d_dcm[i])
        top_left, density = get_positions_and_spacing(patient)
        voxels = get_pixels_hu(patient)

        # Rotate and flip annotations to match volumes
        seg = scipy.ndimage.interpolation.rotate(seg, 90, axes=(2, 0), order=0)
        # Saves data
        resampled_voxels = resample_scan(voxels, resample_shape)
        resampled_labels = resample_label(seg, resample_shape)
        dt_labels = label_distance_transform(resampled_labels)

        # data_out.append(add_scaled_positions(resampled_voxels, resampled_labels, top_left, density))
        data_out.append((resampled_voxels, dt_labels))

    return data_out


def do_dumping(iteration):
    pickle_names = [PICKLES_TRAIN1, PICKLES_TRAIN2, PICKLES_TRAIN3, PICKLES_TRAIN4, PICKLES_TRAIN5, PICKLES_TEST]
    data_train_dirs = [data_train_dir1, data_train_dir2, data_train_dir3, data_train_dir4, data_train_dir5, data_test_dir]
    anns_train_dirs = [anns_train_dir1, anns_train_dir2, anns_train_dir3, anns_train_dir4, anns_train_dir5, anns_test_dir]
    # pickle_names = [PICKLES_TRAIN5]
    # data_train_dirs = [data_train_dir5]
    # anns_train_dirs = [anns_train_dir5]

    pickle_name = pickle_names[iteration]
    data_dir = data_train_dirs[iteration]
    anns_dir = anns_train_dirs[iteration]

    print("Fetching ", pickle_name)
    train = get_dataset(data_dir, anns_dir)

    print("Dumping ", pickle_name)
    hickle.dump(train, pickle_name, mode='w')
    print("Dumped ", pickle_name)

pool = multiprocessing.Pool(3)
pool.map(do_dumping, range(0, 6))
# pool.map(do_dumping, range(0, 1))
# do_dumping(0)

#for i in range(0,6):
#    do_dumping(i)
