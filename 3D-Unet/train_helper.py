import numpy as np
import hickle
import pdb
from data_processing import *
from hyper_params2 import *


def test_prediction2(x_s, y_s, sess, unet):
    predictions = []
    unet_losses = []
    ious = []

    for j in range(len(x_s)):
        x = [x_s[j]]
        y = np.expand_dims([y_s[j]], axis=4)

        unet_train_dict = {
            unet.training: False,
            unet.model_input: x,
            unet.model_labels: y,
        }

        unet_preds, unet_loss = sess.run([unet.predictions, unet.loss], feed_dict=unet_train_dict)
        unet_losses.append(unet_loss)
        predictions.append(unet_preds)

        iou = get_pred_iou([np.squeeze(unet_preds)], [np.squeeze(y)], ret_full=True, reswap=True)
        print('Train IOU: ', iou, 'Mean: ', np.mean(iou[:OUTPUT_CLASSES - 1]))
        ious.append(iou)

    return predictions, np.mean(unet_losses), np.array(ious)


def test_prediction(x_s, y_s, d_s, sess, unet):
    predictions = []
    unet_losses = []
    fc_losses = []
    ious = []

    for j in range(len(x_s)):
        x = x_s[j]
        y = y_s[j]
        d = d_s[j]

        unet_train_dict = {
            unet.training: False,
            unet.model_input: [x[:,:,:,0:1]],
            unet.model_labels: [y],
            unet.model_distances: [d],
        }

        unet_preds, unet_loss = sess.run([unet.predictions, unet.loss], feed_dict=unet_train_dict)
        unet_losses.append(unet_loss)
        predictions.append(unet_preds)

        iou = get_pred_iou([np.squeeze(unet_preds)], [np.squeeze(y)], ret_full=True, reswap=True)
        if np.count_nonzero(y) > 0:
            print('Train IOU: ', iou, 'Mean: ', np.mean(iou[:OUTPUT_CLASSES - 1]))
        ious.append(iou)

    return predictions, np.mean(unet_losses), np.array(ious)

def load_dataset(data, i, force_first=False):
    if i%(4*DATA_SWAP_INTERVAL) == 0 or force_first:
        print("Swaping to dataset 1")
        return hickle.load(PICKLES_TRAIN1)
    elif i%(3*DATA_SWAP_INTERVAL) == 0:
        print("Swaping to dataset 4")
        return hickle.load(PICKLES_TRAIN4)
    elif i%(2*DATA_SWAP_INTERVAL) == 0:
        print("Swaping to dataset 3")
        return hickle.load(PICKLES_TRAIN3)
    elif i%(1*DATA_SWAP_INTERVAL) == 0:
        print("Swaping to dataset 2")
        return hickle.load(PICKLES_TRAIN2)
    return data