import nrrd
import sys
import pdb
import json
import numpy as np

filebase = sys.argv[1]

with open("class_notes.json", "r") as read_file:
    global_dict = json.load(read_file)

with open(f"{filebase}.json", "r") as read_file:
    local_dict = json.load(read_file)

seg, opts = nrrd.read(f"{filebase}_old.nrrd")

new_seg = np.zeros(seg.shape)

for key, value in global_dict.items():
    mask = seg == local_dict[key]
    new_seg[mask] = value

nrrd.write(f"{filebase}.nrrd", new_seg)