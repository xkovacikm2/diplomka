import pandas as pd
import nrrd
import pdb
import sys
from hyper_params import *

filename = sys.argv[1]

seg, opts = nrrd.read(f"{filename}.nrrd")

x, y, z = seg.shape
z = int(z/2)

pd.DataFrame(seg[:,:,z]).to_csv(path_or_buf=f"{filename}.csv", header=False, index=False)