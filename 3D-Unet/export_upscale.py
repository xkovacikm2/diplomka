import sys
import numpy as np
import nrrd
import pdb
from resampling_helpers import *

seg_folder = sys.argv[1]
orig_folder = sys.argv[2]

my_segmentations = list(return_nrrd(seg_folder).values())
dict_dcm = list(return_dcm(orig_folder).values())

for i in range(len(my_segmentations)):
    my_label, _ = nrrd.read(my_segmentations[i])
    my_label = my_label.astype(int)
    patient = load_scan(dict_dcm[i])
    voxels = get_pixels_hu(patient)

    resampled_label = resample_label(my_label, np.array(voxels.shape))
    nrrd.write(f"{seg_folder}/upscaled/{i}.nrrd", resampled_label)