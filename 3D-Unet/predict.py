from unet import *
import hickle
import matplotlib
import sys
import pdb
from hyper_params import *
from data_processing import *
from visualisations import *

test_predictions = hickle.load(PICKLES_TEST)
test_model_path = sys.argv[1]
organ = sys.argv[2]
iteration = sys.argv[3]
test_model_name = f"{MODEL_NAME}_{organ}-{iteration}"

for j in range(len(test_predictions)):
    test_predictions[j] = (test_predictions[j][0], get_single_organ_mask(test_predictions[j][1], organ))

config = tf.ConfigProto()
unet = UNetwork(drop=UNET_DROPOUT, base_filt=UNET_BASE_FILTER, should_pad=UNET_SHOULD_PADDING)  # MODEL DEFINITION

with tf.Session(config=config) as sess:
    print('Loading saved model ...')
    restore_path = f"{test_model_path}/{test_model_name}.meta"
    print(restore_path)
    # restorer = tf.train.import_meta_graph(restore_path)
    saver = tf.train.Saver()
    saver.restore(sess, f"{test_model_path}/{test_model_name}")
    # restorer.restore(sess, f"{SAVE_PATH}/{test_model_name}.ckpt")
    # sess.run(tf.global_variables_initializer())
    print("Model sucessfully restored")

    i = 0

    while i < len(test_predictions):
        x_s, y_s, d_s = get_data_patches_sample([test_predictions[i]], perturbation=False)  # Draw samples from batch
        predictions = []

        for j in range(len(x_s)):
            x = x_s[j]
            y = [np.squeeze(y_s[j])]
 
            test_dict = {
                unet.training: False,
                unet.model_input: [x[:,:,:,0:1]]
            }

            session_result = sess.run([unet.predictions], feed_dict=test_dict)
            preds = [np.squeeze(session_result)]
            predictions.append(preds)

            iou = get_pred_iou(preds, y, ret_full=True, reswap=True)
            iou_out = f"Image {i}, slice {j}, Train IOU: {iou}, Mean: {np.mean(iou[:OUTPUT_CLASSES - 1])}"
            print(iou_out)
            print(iou_out, file=open(f"results/{organ}_results.txt", 'a'))
            plot_confusion_matrix(preds, y, ["background", organ], i, title=f"Heart {i}")

        reconstructed_predictions = reconstruct_from_patches(np.squeeze(predictions))
        # reconstructed_ys = reconstruct_from_patches(np.squeeze(y_s))
        # reconstructed_image = reconstruct_from_patches(np.squeeze(np.array(x_s)[:,:,:,:,0]))

        print_to_nrrd(reconstructed_predictions, f"{PREDICTION_RESULTS_PATH}/nrrd/{i}.nrrd")
        # print_to_nrrd(reconstructed_ys, f"{PREDICTION_RESULTS_PATH}/nrrd/{i}_labels.nrrd")
        # print_to_nrrd(reconstructed_image, f"{PREDICTION_RESULTS_PATH}/nrrd/{i}_orig.nrrd")

        i += 1
