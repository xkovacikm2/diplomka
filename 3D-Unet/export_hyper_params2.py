import sys

RESCALE_SIZE = 120
RESCALE_DEPTH = 120
PATCH_SIZE = RESCALE_SIZE
PATCH_DEPTH = RESCALE_DEPTH

INPUT_SIZE = RESCALE_SIZE
OUTPUT_SIZE = RESCALE_SIZE
INPUT_DEPTH = RESCALE_DEPTH

OFF_IMAGE_FILL = 0  # What to fill an image with if padding is required to make Tensor
OFF_LABEL_FILL = 0  # What to fill a label with if padding is required to make Tensor
INPUT_CLASSES = 2 # Input dimensions
OUTPUT_CLASSES = 2  # Number of output classes in dataset
OUTPUT_DEPTH = PATCH_DEPTH

LEARNING_RATE = 0.001  # Model learning rate
NUM_STEPS = 5001 # Number of train steps per model train
BATCH_SIZE = 1  # Batch - VRAM limited
AUGMENTATION_RATE = 1
UNET_DROPOUT = 0.2
UNET_BASE_FILTER = 10
UNET_SHOULD_PADDING = True
UNET_MAXPOOLING_STRIDE = [2,2,2]

SAVE_PATH = "./tf2"
LOGS_PATH = "./tf_logs2"
TRAINING_RESULTS_PATH = "./training_results2"
PREDICTION_RESULTS_PATH = "./results2"
LOAD_MODEL = True
PICKLES_TEST = f"./export_pickles2/{sys.argv[1]}/test.hkl"
DATA_SWAP_INTERVAL = 21

MODEL_NAME = 'model'

#### prepare pickles only
data_test_dir = './export_data/test/'
segm_test_dir = f"./export_data/test-inter-segm/{sys.argv[1]}/upscaled"
