import os
import hickle
import nrrd
import numpy as np
import pydicom as dicom
import scipy.ndimage
import scipy.spatial
import multiprocessing
import pdb
import sys
from export_data_processing2 import *
from resampling_helpers import *
from export_hyper_params2 import *


def get_bbox(image):
    hit_layers = np.nonzero(image)
    bbox = []

    for layer in hit_layers:
        bbox.append(slice(min(layer), max(layer)))

    return bbox


def zoom_bbox(bbox, max_shape, factor=0.1):
    new_bbox = []

    for i in range(len(bbox)):
        start = bbox[i].start
        start = int(start * (1 - factor)) 

        if start < 0:
            start = 0

        stop = bbox[i].stop
        stop = int(stop * (1 + factor))

        if stop >= max_shape[i]:
            stop = max_shape[i] - 1

        new_bbox.append(slice(start, stop))
    return new_bbox


def resample_scan(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)
    return image


def resample_label(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.zoom(image, real_resize_factor, prefilter=False, mode='nearest', order=0)
    return image


def get_positions_and_spacing(patient):
    top_left_coordinate = [float(i) for i in patient[0].ImagePositionPatient]

    density = [
        float(patient[0].PixelSpacing[0])*patient[0].Rows, float(patient[1].PixelSpacing[1])*patient[1].Columns,
        abs(abs(int(patient[0].SliceLocation)) - abs(int(patient[-1].SliceLocation)))
    ]

    return top_left_coordinate, density


def label_distance_transform(labels):
    m_c_labels = [labels]
    classes = np.unique(labels).tolist()
    classes.remove(0) #ignore background

    for class_num in classes:
        inv_labels = np.ones(labels.shape).astype(int)
        inv_labels[labels==class_num] = 0
        distances = edt.edt(inv_labels, black_border=False)
        m_c_labels.append(distances)
        
    return m_c_labels


exp_folder = sys.argv[2]
organ = sys.argv[1]

data_dir = f"{exp_folder}/{organ}"
d_dcm = list(return_dcm(data_test_dir).values())
d_segm = list(return_nrrd(segm_test_dir).values())
d_segm2 = list(return_nrrd(f"{data_dir}").values())

for i in range(len(d_dcm)):
    print(f"Preparing: {i}")

    my_seg, _ = nrrd.read(d_segm[i])
    my_seg = my_seg.astype(int)
    my_seg2, _ = nrrd.read(d_segm2[i])
    my_seg2 = my_seg2.astype(int)
    patient = load_scan(d_dcm[i])
    voxels = get_pixels_hu(patient)

    # Get BBox and zoom it a little to keep some varying space
    mask = get_bbox(my_seg)
    mask = zoom_bbox(mask, my_seg.shape, 0.1)

    # Calculate new shape from bbox
    resample_shape = np.array([mask[i].stop - mask[i].start for i in range(len(mask))])

    # Push crop to orig size
    resampled_my_seg2 = resample_label(my_seg2, resample_shape)
    new_seg = np.zeros(voxels.shape)
    new_seg[mask] = resampled_my_seg2

    # Serialize
    nrrd.write(f"{data_dir}/labels/upscaled/{i}_labels.nrrd", new_seg)
    rotated = scipy.ndimage.interpolation.rotate(new_seg, -90, axes=(2, 0), order=0)
    nrrd.write(f"{data_dir}/labels/upscaled/rotated/{i}_rotated.nrrd", rotated)
    nrrd.write(f"{data_dir}/labels/upscaled/{i}_orig.nrrd", voxels)

