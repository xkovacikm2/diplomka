from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.utils.multiclass import unique_labels
import numpy as np
import nrrd
import pdb

def plot_slice(slice_in, is_anns = False, num_anns = 4, to_file = False):
    # Plot a slice of data - can either be raw image data or corresponding annotations
    slice_in = np.squeeze(slice_in)
    plt.figure()
    plt.set_cmap(plt.bone())

    if is_anns:
        plt.pcolormesh(slice_in, vmin = 0, vmax = num_anns - 1)
    else:
        plt.pcolormesh(slice_in)

    if not to_file:
        plt.show()


def print_to_nrrd(data, filename):
    nrrd.write(filename, data)


def plot_loss(unet_loss, dist_pun, iteration):
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.stackplot(list(range(len(unet_loss))), unet_loss, dist_pun, baseline='zero', labels=['dice loss', 'distance loss'])
    ax1.set_yscale("log")
    plt.legend(loc='upper right')
    plt.savefig(f"results/loss_{iteration}.png")


def plot_loss(unet_loss, iteration):
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.plot(unet_loss, color='blue', label='unet loss')
    plt.legend(loc='upper right')
    plt.savefig(f"results/loss_{iteration}.png")


def plot_iou(bg_iou, organ_iou, organ_name, iteration):
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    ax1.plot(bg_iou, color='blue', label='background iou')
    ax1.plot(organ_iou, color='red', label=f"{organ_name} iou")
    plt.legend(loc='lower right')
    plt.savefig(f"results/iou_{iteration}.png")


def plot_confusion_matrix(y_true_orig, y_pred_orig, classes, iteration,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'

    y_true = np.array(y_true_orig).flatten()
    y_pred = np.array(y_pred_orig).flatten()

    # Compute confusion matrix
    cm = confusion_matrix(y_true, y_pred)
    # Only use the labels that appear in the data
    # classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")

    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    plt.savefig(f"results/confusion_matrix_{iteration}.png")
    return ax
