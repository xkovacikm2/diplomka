import os
import pydicom as dicom
import scipy.ndimage
import numpy as np


def get_pixels_hu(scans):
    image = np.stack([s.pixel_array for s in scans])
    # Convert to int16 (from sometimes int16),
    # should be possible as values should always be low enough (<32k)
    image = image.astype(np.int16)

    # Set outside-of-scan pixels to 1
    # The intercept is usually -1024, so air is approximately 0
    # image[image == -2000] = 0

    # Convert to Hounsfield units (HU)
    intercept = scans[0].RescaleIntercept
    slope = scans[0].RescaleSlope

    if slope != 1:
        image = slope * image.astype(np.float64)
        image = image.astype(np.int16)

    image += np.int16(intercept)

    return np.array(image, dtype=np.int16)


def return_nrrd(file_path):
    # Reads all NRRD (annotation) files within a directory
    out_nrrd = {}
    for dirName, subdirList, fileList in os.walk(file_path):
        for filename in fileList:
            lower_filename = filename.lower()
            if ".nrrd" in lower_filename and "_old.nrrd" not in lower_filename:
                name = filename.split('_')[0]
                name = name.split('.')[0]  # Get annotation name and store in dictionary
                out_nrrd[name] = os.path.join(dirName, filename)
    return out_nrrd


def return_dcm(file_path, check_term='LCTSC'):
    # Reads all DCM (slices) files within a directory and order the files correctly (based on filename)
    out_dcm = {}
    for dirName, subdirList, fileList in os.walk(file_path):
        c_dcm = []
        cur_name = ""
        dir_split = dirName.split("/")
        for f_chk in dir_split:
            if check_term in f_chk:
                cur_name = f_chk
        for filename in fileList:
            if ".dcm" in filename.lower():
                name = int(os.path.splitext(filename)[0])
                c_dcm.append((os.path.join(dirName, filename), name))
        if len(c_dcm) > 0:
            c_dcm = sorted(c_dcm, key=lambda t: t[1])  # Sort into correct order
            out_dcm[cur_name] = [c[0] for c in c_dcm]  # Store in dictionary
    return out_dcm


def load_scan(path):
    slices = [dicom.read_file(s) for s in path]
    slices.sort(key=lambda x: int(-x.SliceLocation))

    return slices


def resample_scan(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)
    return image


def resample_label(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.zoom(image, real_resize_factor, prefilter=False, mode='nearest', order=0)
    return image

