RESCALE_SIZE = 240
RESCALE_DEPTH = 60
# Let this always be divider of rescale size
PATCH_SIZE = 240
PATCH_DEPTH = 60

INPUT_SIZE = PATCH_SIZE 
OUTPUT_SIZE = PATCH_SIZE
INPUT_DEPTH = PATCH_DEPTH

OFF_IMAGE_FILL = 0  # What to fill an image with if padding is required to make Tensor
OFF_LABEL_FILL = 0  # What to fill a label with if padding is required to make Tensor
INPUT_CLASSES = 2 # Input dimensions
OUTPUT_CLASSES = 2  # Number of output classes in dataset
OUTPUT_DEPTH = PATCH_DEPTH

LEARNING_RATE = 0.001  # Model learning rate
NUM_STEPS = 10001 # Number of train steps per model train
BATCH_SIZE = 1  # Batch - VRAM limited
AUGMENTATION_RATE = 1
UNET_DROPOUT = 0.2
UNET_BASE_FILTER = 10
UNET_SHOULD_PADDING = True
UNET_MAXPOOLING_STRIDE = [2,2,2]

SAVE_PATH = "./tf"
LOGS_PATH = "./tf_logs"
TRAINING_RESULTS_PATH = "./training_results"
PREDICTION_RESULTS_PATH = "./results"
LOAD_MODEL = True
PICKLES_TRAIN1 = "./pickles/train1.hkl"
PICKLES_TRAIN2 = "./pickles/train2.hkl"
PICKLES_TRAIN3 = "./pickles/train3.hkl"
PICKLES_TRAIN4 = "./pickles/train4.hkl"
PICKLES_TRAIN5 = "./pickles/train5.hkl"
PICKLES_TEST = "./pickles/test.hkl"
DATA_SWAP_INTERVAL = 21

MODEL_NAME = 'model'  # Model name to LOAD FROM (LOOKS IN SAVE_PATH DIRECTORY)

#### prepare pickles only
data_test_dir = './data/test/'
anns_test_dir = './data/test-segm/'

data_train_dir1 = './data/train/1/'
anns_train_dir1 = './data/train-segm/1/'
data_train_dir2 = './data/train/2/'
anns_train_dir2 = './data/train-segm/2/'
data_train_dir3 = './data/train/3/'
anns_train_dir3 = './data/train-segm/3/'
data_train_dir4 = './data/train/4/'
anns_train_dir4 = './data/train-segm/4/'
data_train_dir5 = './data/train/5/'
anns_train_dir5 = './data/train-segm/5/'
