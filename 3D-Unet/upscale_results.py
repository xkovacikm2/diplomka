import sys
import numpy as np
import nrrd
import pdb
from resampling_helpers import *
from data_processing import get_pred_iou, organ_encoding

organ = sys.argv[1]
seg_folder = sys.argv[2]
orig_folder = sys.argv[3]

my_segmentations = list(return_nrrd(seg_folder).values())
truth = list(return_nrrd(orig_folder).values())

for i in range(len(my_segmentations)):
    my_label, _ = nrrd.read(my_segmentations[i])
    truth_label, _ = nrrd.read(truth[i])
    my_label = my_label.astype(int)
    truth_label = truth_label.astype(int)
    rotated_truth = scipy.ndimage.interpolation.rotate(truth_label, 90, axes=(2,0), order=0)
    single_organ_truth = np.zeros(rotated_truth.shape)
    single_organ_truth[rotated_truth == organ_encoding()[organ]] = 1

    resampled_label = resample_label(my_label, np.array(single_organ_truth.shape))
    iou = get_pred_iou([np.array(resampled_label)], [np.array(single_organ_truth)], ret_full=True, reswap=True)
    print(f"{i}: {iou}")
    nrrd.write(f"{seg_folder}/upscaled/{i}.nrrd", resampled_label)