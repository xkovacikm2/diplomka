import os
import hickle
import nrrd
import numpy as np
import pydicom as dicom
import scipy.ndimage
import scipy.spatial
import multiprocessing
import pdb
import sys
from data_processing import *
from resampling_helpers import *
from hyper_params2 import *


def get_bbox(image):
    mask = image == 0
    bbox = []
    all_axis = np.arange(image.ndim)
    for kdim in all_axis:
        nk_dim = np.delete(all_axis, kdim)
        mask_i = mask.all(axis=tuple(nk_dim))
        dmask_i = np.diff(mask_i)
        idx_i = np.nonzero(dmask_i)[0]
        if len(idx_i) != 2:
            idx_i = [idx_i[0], idx_i[-1]]
        bbox.append(slice(idx_i[0]+1, idx_i[1]+1))
    return bbox


def zoom_bbox(bbox, max_shape, factor=0.1):
    new_bbox = []
    for i in range(len(bbox)):
        start = bbox[i].start
        start = int(start * (1 - factor)) 
        if start < 0:
            start = 0

        stop = bbox[i].stop
        stop = int(stop * (1 + factor))
        if stop >= max_shape[i]:
            stop = max_shape[i] - 1

        new_bbox.append(slice(start, stop))
    return new_bbox



def resample_scan(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.interpolation.zoom(image, real_resize_factor)
    return image


def resample_label(image, new_shape):
    real_resize_factor = new_shape / image.shape
    image = scipy.ndimage.zoom(image, real_resize_factor, prefilter=False, mode='nearest', order=0)
    return image


def get_positions_and_spacing(patient):
    top_left_coordinate = [float(i) for i in patient[0].ImagePositionPatient]

    density = [
        float(patient[0].PixelSpacing[0])*patient[0].Rows, float(patient[1].PixelSpacing[1])*patient[1].Columns,
        abs(abs(int(patient[0].SliceLocation)) - abs(int(patient[-1].SliceLocation)))
    ]

    return top_left_coordinate, density


def label_distance_transform(labels):
    m_c_labels = [labels]
    classes = np.unique(labels).tolist()
    classes.remove(0) #ignore background

    for class_num in classes:
        inv_labels = np.ones(labels.shape).astype(int)
        inv_labels[labels==class_num] = 0
        distances = edt.edt(inv_labels, black_border=False)
        m_c_labels.append(distances)
        
    return m_c_labels


exp_folder = sys.argv[2]
organ = sys.argv[1]

data_dir = f"{exp_folder}/{organ}"
dict_dcm = return_dcm(data_test_dir)
dict_nrrd = return_nrrd(anns_test_dir)
d_segm = list(return_nrrd(segm_test_dir).values())
d_segm2 = list(return_nrrd(f"{data_dir}/results2/labels_raw").values())
keys = list(dict_nrrd.keys())
d_dcm = []
d_nrrd = []

for key in keys:
    d_dcm.append(dict_dcm[key])
    d_nrrd.append(dict_nrrd[key])


for i in range(len(d_nrrd)):
    print(f"Preparing: {i}")

    gt_seg, _ = nrrd.read(d_nrrd[i])
    gt_seg = gt_seg.astype(int)
    gt_seg[gt_seg != organ_encoding()[organ]] = 0
    gt_seg[gt_seg == organ_encoding()[organ]] = 1
    my_seg, _ = nrrd.read(d_segm[i])
    my_seg = my_seg.astype(int)
    my_seg2, _ = nrrd.read(d_segm2[i])
    my_seg2 = my_seg2.astype(int)
    patient = load_scan(d_dcm[i])
    voxels = get_pixels_hu(patient)

    # Rotate and flip annotations to match volumes
    gt_seg = scipy.ndimage.interpolation.rotate(gt_seg, 90, axes=(2, 0), order=0)

    # Get BBox and zoom it a little to keep some varying space
    mask = get_bbox(my_seg)
    mask = zoom_bbox(mask, my_seg.shape, 0.1)

    # Calculate new shape from bbox
    resample_shape = np.array([mask[i].stop - mask[i].start for i in range(len(mask))])

    # Push crop to orig size
    resampled_my_seg2 = resample_label(my_seg2, resample_shape)
    new_seg = np.zeros(gt_seg.shape)
    new_seg[mask] = resampled_my_seg2

    # Measurements
    iou = get_pred_iou([new_seg], [gt_seg], ret_full=True, reswap=True)
    print(f"Image {i}, iou: {iou}")
    print(f"Image {i}, iou: {iou}", file=open(f"results3/{organ}_results.txt", 'a'))

    # Serialize
    nrrd.write(f"{data_dir}/results2/labels_upscaled/{i}_labels.nrrd", new_seg)
    new_seg = scipy.ndimage.interpolation.rotate(new_seg, -90, axes=(2, 0), order=0)
    nrrd.write(f"{data_dir}/results2/labels_upscaled/rotated/{i}_rotated.nrrd", new_seg)
    nrrd.write(f"{data_dir}/results2/labels_upscaled/{i}_orig.nrrd", voxels)

