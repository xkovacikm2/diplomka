import sys

RESCALE_SIZE = 120
RESCALE_DEPTH = 120
PATCH_SIZE = RESCALE_SIZE
PATCH_DEPTH = RESCALE_DEPTH

INPUT_SIZE = RESCALE_SIZE
OUTPUT_SIZE = RESCALE_SIZE
INPUT_DEPTH = RESCALE_DEPTH

OFF_IMAGE_FILL = 0  # What to fill an image with if padding is required to make Tensor
OFF_LABEL_FILL = 0  # What to fill a label with if padding is required to make Tensor
INPUT_CLASSES = 2 # Input dimensions
OUTPUT_CLASSES = 2  # Number of output classes in dataset
OUTPUT_DEPTH = PATCH_DEPTH

LEARNING_RATE = 0.001  # Model learning rate
NUM_STEPS = 5001 # Number of train steps per model train
BATCH_SIZE = 1  # Batch - VRAM limited
AUGMENTATION_RATE = 1
UNET_DROPOUT = 0.2
UNET_BASE_FILTER = 10
UNET_SHOULD_PADDING = True
UNET_MAXPOOLING_STRIDE = [2,2,2]

SAVE_PATH = "./tf2"
LOGS_PATH = "./tf_logs2"
TRAINING_RESULTS_PATH = "./training_results2"
PREDICTION_RESULTS_PATH = "./results2"
LOAD_MODEL = True
PICKLES_TRAIN1 = "./pickles2/train1.hkl"
PICKLES_TRAIN2 = "./pickles2/train2.hkl"
PICKLES_TRAIN3 = "./pickles2/train3.hkl"
PICKLES_TRAIN4 = "./pickles2/train4.hkl"
PICKLES_TRAIN5 = "./pickles2/train5.hkl"
PICKLES_TEST = "./pickles2/test.hkl"
DATA_SWAP_INTERVAL = 21

MODEL_NAME = 'model'

#### prepare pickles only
data_test_dir = './data/test/'
anns_test_dir = './data/test-segm/'
segm_test_dir = f"./data/test-inter-segm/{sys.argv[1]}/upscaled"

data_train_dir1 = './data/train/1/'
anns_train_dir1 = './data/train-segm/1/'
segm_train_dir1 = f"./data/train-inter-segm/{sys.argv[1]}-1/upscaled"
data_train_dir2 = './data/train/2/'
anns_train_dir2 = './data/train-segm/2/'
segm_train_dir2 = f"./data/train-inter-segm/{sys.argv[1]}-2/upscaled"
data_train_dir3 = './data/train/3/'
anns_train_dir3 = './data/train-segm/3/'
segm_train_dir3 = f"./data/train-inter-segm/{sys.argv[1]}-3/upscaled"
data_train_dir4 = './data/train/4/'
anns_train_dir4 = './data/train-segm/4/'
segm_train_dir4 = f"./data/train-inter-segm/{sys.argv[1]}-4/upscaled"
data_train_dir5 = './data/train/5/'
anns_train_dir5 = './data/train-segm/5/'
segm_train_dir5 = f"./data/train-inter-segm/{sys.argv[1]}-5/upscaled"
