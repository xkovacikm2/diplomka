import pdb
import random
import sys
from data_processing import *
from unet import *
from visualisations import *
from train_helper import *

organ_name, is_expert = (sys.argv[1], True)
restore_iter = int(sys.argv[2])

if restore_iter == 0:
    tf.reset_default_graph()
    tf.random.set_random_seed(42)

unet = UNetwork(drop=UNET_DROPOUT, base_filt=UNET_BASE_FILTER, should_pad=UNET_SHOULD_PADDING)  # MODEL DEFINITION
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
iou_bg = []
iou_organ = []
unet_validation_losses = []
fully_connected_validation_losses = []
train_run = []

with tf.Session(config=config) as sess:

    if restore_iter > 0:
        TEST_MODEL_NAME = f"{MODEL_NAME}_{organ_name}-{restore_iter}"
        print(f"Restoring {TEST_MODEL_NAME}")
        saver = tf.train.Saver()
        saver.restore(sess, f"{SAVE_PATH}/{TEST_MODEL_NAME}")
        print("Restored")
    else:
        print("No save defined, starting anew")
        init = tf.global_variables_initializer()
        saver = tf.train.Saver(tf.global_variables())
        sess.run(init)

    writer = tf.summary.FileWriter(LOGS_PATH, graph=tf.get_default_graph())
    unet_t_loss = []
    fc_t_loss = []

    for i in range(restore_iter, NUM_STEPS):
        unet_ep_loss = []
        fc_ep_loss = []
        print('Epizode: ', i)

        train_run = load_dataset(train_run, i, force_first=(i==restore_iter))

        if is_expert and (i%DATA_SWAP_INTERVAL == 0 or i == restore_iter):
            for j in range(len(train_run)):
                train_run[j] = (train_run[j][0], get_single_organ_mask(train_run[j][1], organ_name))

        k = i%len(train_run)
        x_s, y_s, d_s = get_data_patches_sample([train_run[k]])  # Draw samples from batch

        for j in range(0, len(x_s), BATCH_SIZE):
            x = x_s[j:j+BATCH_SIZE]
            y = y_s[j:j+BATCH_SIZE]
            d = d_s[j:j+BATCH_SIZE]

            if np.count_nonzero(y) > 0 or random.random() > 0.9:
                ## unet training
                unet_train_dict = {
                    unet.training: True,
                    unet.model_input: x,
                    unet.model_labels: y,
                    unet.model_distances: d
                }

                _, uloss, dist_pun = sess.run([unet.train_op, unet.dice_loss, unet.dist_pun], feed_dict=unet_train_dict) 
                unet_ep_loss.append(uloss)  # Loss store
                fc_ep_loss.append(dist_pun)

        unet_t_loss.append(np.mean(unet_ep_loss))
        fc_t_loss.append(np.mean(fc_ep_loss))

        if i % 100 == 0 and i > 0:
            print('Saving model at iter: ', i)  # Save periodically
            saver.save(sess, f"{SAVE_PATH}/{MODEL_NAME}_{organ_name}", global_step=i)

        if i % 10 == 0:
            # print(f"Iteration: {i}, Unet Loss: {np.mean(unet_t_loss)}, FC loss: {np.mean(fc_t_loss)}")  # Get periodic progress reports
            print(f"Iteration: {i}, Unet Loss: {np.mean(unet_t_loss)}, Distance punish loss: {np.mean(fc_t_loss)}")  # Get periodic progress reports
            predictions = []
            x_s, y_s, d_s = get_data_patches_sample(train_run, perturbation=False)
            _, __, iou = test_prediction(x_s, y_s, d_s, sess, unet)
        if i % 100 == 0:
            print("Evaluation")
            train_run = hickle.load(PICKLES_TRAIN5)
            bg = []
            og = []

            for j in range(len(train_run)):
                train_run[j] = (train_run[j][0], get_single_organ_mask(train_run[j][1], organ_name))
                x_s, y_s, d_s = get_data_patches_sample([train_run[j]], perturbation=False)

                predictions, u_loss, iou = test_prediction(x_s, y_s, d_s, sess, unet)
                bg.append(np.mean(iou[:,0]))
                og.append(np.mean(iou[:,1]))

                if i == 0:
                    reconstructed_image = reconstruct_from_patches(np.squeeze(np.array(x_s)[:,:,:,:,0]))
                    print_to_nrrd(reconstructed_image, f"{TRAINING_RESULTS_PATH}/nrrd/{j}_orig.nrrd")
                    print_to_nrrd(reconstruct_from_patches(np.squeeze(y_s)), f"{TRAINING_RESULTS_PATH}/nrrd/{j}_y.nrrd")

                print('prediction nrrd print')
                reconstructed_predictions = reconstruct_from_patches(np.squeeze(predictions))
                print_to_nrrd(reconstructed_predictions, f"{TRAINING_RESULTS_PATH}/nrrd/{i}_{j}_predict.nrrd")
                print('printed')

            iou_organ.append(np.mean(og))
            iou_bg.append(np.mean(bg))
            train_run = load_dataset(train_run, i, force_first=(i == restore_iter))
            plot_loss(unet_t_loss, fc_t_loss, i)
            plot_iou(iou_bg, iou_organ, organ_name, i)
