import random
import scipy
import numpy as np
import edt
import pdb
from export_hyper_params2 import *


def organ_encoding():
    return {
        "left_lung": 1,
        "right_lung": 2,
        "hearth": 3,
        "esophagus": 4,
        "spine": 5,
        "background": 0
    }


def get_single_organ_mask2(labels, organ_name):
    pdb.set_trace()
    new_labels = np.zeros(labels.shape)
    encoding = organ_encoding()
    new_labels[labels != encoding[organ_name]] = 0
    new_labels[labels == encoding[organ_name]] = 1
    return new_labels.astype(int)


def get_single_organ_mask(labels, organ_name):
    new_labels = [np.copy(labels[0])]
    encoding = organ_encoding()
    new_labels[0][labels[0] != encoding[organ_name]] = 0
    new_labels[0][labels[0] == encoding[organ_name]] = 1
    new_labels.append(labels[encoding[organ_name]])
    return np.array(new_labels).astype(int) 


def get_raw_voxel_value(voxels):
    return voxels[:,:,:,0]


def add_positions_to_raw_voxels(raw_voxels, voxels_w_positions):
    expanded_voxels = np.expand_dims(raw_voxels, axis=3)
    return np.append(expanded_voxels[:,:,:,:], voxels_w_positions[:,:,:,1:4], axis=3)


def rotate_data(voxels, lbls, theta=None):
    # Rotates volume by a minor angle (+,- 10 degrees)
    if theta is None:
        theta = random.randint(-10, 10)
    # vox_new = scipy.ndimage.interpolation.rotate(get_raw_voxel_value(voxels), theta, reshape=False)
    vox_new = scipy.ndimage.interpolation.rotate(voxels, theta, reshape=False)
    lbl_new = scipy.ndimage.interpolation.rotate(lbls, theta, reshape=False)
    # return add_positions_to_raw_voxels(vox_new, voxels), lbl_new
    return vox_new, lbl_new


def scale_and_crop_centre(orig_voxels, lbls):
    # Scales the volume by a minor size and crops around centre (can also modify for random crop)
    # voxels = get_raw_voxel_value(orig_voxels)
    voxels = orig_voxels
    o_s = voxels.shape
    r_s = [0] * len(o_s)
    scale_factor = random.uniform(1, 1.2)
    vox_zoom = scipy.ndimage.interpolation.zoom(voxels, scale_factor, order=1)
    lbl_zoom = scipy.ndimage.interpolation.zoom(lbls, scale_factor, order=0)
    new_shape = vox_zoom.shape
    # Start with offset
    for i in range(len(o_s)):
        if new_shape[i] == 1:
            r_s[i] = 0
            continue
        r_c = int(((new_shape[i] - o_s[i]) - 1) / 2)
        r_s[i] = r_c
    r_e = [r_s[i] + o_s[i] for i in list(range(len(o_s)))]
    vox_zoom = vox_zoom[r_s[0]:r_e[0], r_s[1]:r_e[1], r_s[2]:r_e[2]]
    lbl_zoom = lbl_zoom[r_s[0]:r_e[0], r_s[1]:r_e[1], r_s[2]:r_e[2]]
    # return add_positions_to_raw_voxels(vox_zoom, orig_voxels), lbl_zoom
    return vox_zoom, lbl_zoom


def grayscale(voxels, lbls):
    # Introduce a random global increment in gray-level value of volume.
    # Gray values cannot be lower than 0
    aug = random.randint(-10, 10)
    smp = np.random.normal(0, 1, size=np.shape(voxels))
    voxels = voxels + aug * smp
    # voxels[voxels <= 0] = 0
    # voxels[voxels > 500] = 500  # Technically, need to cap max as well
    return voxels, lbls


def sample_with_p(p):
    # Helper function to return boolean of a sample with given probability p
    if random.random() < p:
        return True
    else:
        return False


def get_random_perturbation(voxels, lbls):
    # Generate a random perturbation of the input feature + label (and makes sure they're consistent)
    p_rotate = 0.6
    p_scale = 0.5
    p_gray = 0.6
    cur_vox, cur_lbl = voxels, lbls
    if sample_with_p(p_rotate):
        cur_vox, cur_lbl = rotate_data(cur_vox, cur_lbl)
    if sample_with_p(p_scale):
        cur_vox, cur_lbl = scale_and_crop_centre(cur_vox, cur_lbl)
    if sample_with_p(p_gray):
        cur_vox, cur_lbl = grayscale(cur_vox, cur_lbl)
    return cur_vox, cur_lbl


def get_scaled_input(data):
    min_i=RESCALE_SIZE
    min_o=RESCALE_SIZE
    depth=RESCALE_SIZE
    depth_out=RESCALE_SIZE
    image_fill=OFF_IMAGE_FILL
    label_fill=OFF_LABEL_FILL
    n_classes=OUTPUT_CLASSES
    norm_max=500
    # Takes raw data (x, y) and scales to match desired input and output sizes to feed into Tensorflow
    # Pads and normalises input and also moves axes around to orientation expected by tensorflow

    input_scale_factor = min_i / data[0].shape[0]
    output_scale_factor = min_o / data[0].shape[0]

    vox_zoom = None
    lbl_zoom = None

    if not input_scale_factor == 1:
        vox_zoom = scipy.ndimage.interpolation.zoom(data[0], input_scale_factor, order=1)
        # Order 1 is bilinear - fast and good enough
    else:
        vox_zoom = data[0]

    if not output_scale_factor == 1:
        lbl_zoom = scipy.ndimage.interpolation.zoom(data[1], output_scale_factor, order=0)
        # Order 0 is nearest neighbours: VERY IMPORTANT as it ensures labels are scaled properly (and stay discrete)
    else:
        lbl_zoom = data[1]

    lbl_pad = label_fill * np.ones((min_o, min_o, depth_out - lbl_zoom.shape[-1]))
    lbl_zoom = np.concatenate((lbl_zoom, lbl_pad), 2)
    lbl_zoom = lbl_zoom[np.newaxis, :, :, :]

    vox_pad = image_fill * np.ones((min_i, min_i, depth - vox_zoom.shape[-1]))
    vox_zoom = np.concatenate((vox_zoom, vox_pad), 2)

    max_val = np.max(vox_zoom)
    if not np.max(vox_zoom) == 0:
        vox_zoom = vox_zoom * norm_max / np.max(vox_zoom)

    vox_zoom = vox_zoom[np.newaxis, :, :, :]

    vox_zoom = np.swapaxes(vox_zoom, 0, -1)
    lbl_zoom = np.swapaxes(lbl_zoom, 0, -1)
    # Swap axes

    return vox_zoom, lbl_zoom


def get_dataset_sample(data, size, no_perturb=False):
    # If augmenting on test and using original dataset, process and return augmented samples from dataset
    # NOTE: this approach is v. v. slow
    x_y_data = random.sample(data, size)
    x = []
    y = []
    orig_y = []
    for entry in x_y_data:
        x_cur, y_cur = get_random_perturbation(entry[0], entry[1])
        if no_perturb:
            x_cur, y_cur = entry
        orig_y.append(np.copy(y_cur))
        x_cur, y_cur = get_scaled_input((x_cur, y_cur))
        x.append(x_cur)
        y.append(y_cur)
    return x, y, orig_y


def get_data_raw_sample(data, size):
    # If augmentation done in pre-processing, simply sample from your data
    x_y_data = random.sample(data, size)
    return [x[0] for x in x_y_data], [y[1] for y in x_y_data]


def get_data_patches_sample_wo_dist(data, size=1, perturbation=True):
    orig_data = random.sample(data, size)
    scans = []
    annotations = []

    for j in range(0, size):
        x_y_data = [(orig_data[j])]

        if perturbation:
            for i in range(AUGMENTATION_RATE):
                tmp = get_random_perturbation(orig_data[j][0], orig_data[j][1])
                x_y_data.append((tmp[0], tmp[1]))

        for i in range(len(x_y_data)):
            for x_i in range(0, RESCALE_DEPTH, PATCH_DEPTH):
                for y_i in range(0, RESCALE_SIZE, PATCH_SIZE):
                    for z_i in range(0, RESCALE_SIZE, PATCH_SIZE):
                        scan = x_y_data[0][x_i:x_i+PATCH_DEPTH, y_i:y_i+PATCH_SIZE, z_i:z_i+PATCH_SIZE]

                        scans.append(scan)

    return scans


def get_data_patches_sample(data, size=1, perturbation=True):
    orig_data = random.sample(data, size)
    scans = []
    annotations = []
    distances = []

    for j in range(0, size):
        x_y_data = [(orig_data[j][0], orig_data[j][1][0], orig_data[j][1][1])]

        if perturbation:
            for i in range(AUGMENTATION_RATE):
                tmp = get_random_perturbation(orig_data[j][0], orig_data[j][1][0])
                dt_mask = np.copy(tmp[1])
                dt_mask[dt_mask == 0] = 2
                dt_mask[dt_mask == 1] = 0
                dt_mask[dt_mask == 2] = 1
                x_y_data.append((tmp[0], tmp[1], edt.edt(dt_mask, black_border=False)))

        for i in range(len(x_y_data)):
            for x_i in range(0, RESCALE_DEPTH, PATCH_DEPTH):
                for y_i in range(0, RESCALE_SIZE, PATCH_SIZE):
                    for z_i in range(0, RESCALE_SIZE, PATCH_SIZE):
                        scan = np.expand_dims(x_y_data[i][0][x_i:x_i+PATCH_DEPTH, y_i:y_i+PATCH_SIZE, z_i:z_i+PATCH_SIZE], axis=3)
                        annotation = np.expand_dims(x_y_data[i][1][x_i:x_i+PATCH_DEPTH, y_i:y_i+PATCH_SIZE, z_i:z_i+PATCH_SIZE], axis=3)
                        distance = np.expand_dims(x_y_data[i][2][x_i:x_i+PATCH_DEPTH, y_i:y_i+PATCH_SIZE, z_i:z_i+PATCH_SIZE], axis=3)

                        scans.append(scan)
                        annotations.append(annotation)
                        distances.append(distance)

    return scans, annotations, distances


def reconstruct_from_patches(patches):
    wpatches = patches
    patches_len = int(RESCALE_SIZE/PATCH_SIZE)
    prediction = np.zeros((RESCALE_DEPTH, RESCALE_SIZE, RESCALE_SIZE), dtype=np.int)

    if(len(patches.shape) == 3):
        wpatches = [patches]

    patch_idx = 0

    for x_i in range(0, int(RESCALE_DEPTH/PATCH_DEPTH)):
        for y_i in range(0, patches_len):
            for z_i in range(0,  patches_len):
                pred_x_i = x_i * PATCH_DEPTH
                pred_y_i = y_i * PATCH_SIZE
                pred_z_i = z_i * PATCH_SIZE
                prediction[pred_x_i:pred_x_i+PATCH_DEPTH, pred_y_i:pred_y_i+PATCH_SIZE, pred_z_i:pred_z_i+PATCH_SIZE] = wpatches[patch_idx][:,:,:]
                patch_idx += 1

    return prediction


def get_pred_iou(predictions, lbl_original, ret_full=False, reswap=False):
    # Get mean_iou for full batch
    iou = []
    for i in range(len(lbl_original)):
        pred_cur = np.squeeze(predictions[i])
        metric = get_mean_iou(pred_cur, lbl_original[i], ret_full=ret_full, reswap=reswap)
        iou.append(metric)
    if ret_full:
        return np.mean(iou, axis=0)
    else:
        return np.mean(iou)


def swap_axes(pred):
    pred = np.swapaxes(pred, -1, 0)
    pred = np.squeeze(pred)
    return pred


def get_mean_iou(pred, lbl_original, ret_full=False, reswap=False):
    num_classes=OUTPUT_CLASSES
    # Get mean IOU between input predictions and target labels. Note, method implicitly resizes as needed
    # Ret_full - returns the full iou across all classes
    # Reswap - if lbl_original is in tensorflow format, swap it back into the format expected by plotting tools (+ format of raw data)

    # Swap axes back
    pred = swap_axes(pred)
    if reswap:
        lbl_original = swap_axes(lbl_original)
    pred_upscale = upscale_segmentation(pred, np.shape(lbl_original))
    iou = [1] * num_classes
    for i in range(num_classes):
        test_shape = np.zeros(np.shape(lbl_original))
        test_shape[pred_upscale == i] = 1
        test_shape[lbl_original == i] = 1
        full_sum = int(np.sum(test_shape))
        test_shape = -1 * np.ones(np.shape(lbl_original))
        test_shape[lbl_original == i] = pred_upscale[lbl_original == i]
        t_p = int(np.sum(test_shape == i))
        if not full_sum == 0:
            iou[i] = t_p / full_sum
    if ret_full:
        return iou
    else:
        return np.mean(iou)


def upscale_segmentation(lbl, shape_desired):
    # Returns scaled up label for a given input label and desired shape. Required for Mean IOU calculation

    scale_factor = shape_desired[0] / lbl.shape[0]

    lbl_upscale = scipy.ndimage.interpolation.zoom(lbl, scale_factor, order=0)
    # Order 0 EVEN more important here
    lbl_upscale = lbl_upscale[:, :, :shape_desired[-1]]
    if lbl_upscale.shape[-1] < shape_desired[-1]:
        pad_zero = OFF_LABEL_FILL * np.zeros(
            (shape_desired[0], shape_desired[1], shape_desired[2] - lbl_upscale.shape[-1]))
        lbl_upscale = np.concatenate((lbl_upscale, pad_zero), axis=-1)
    return lbl_upscale


def get_label_accuracy(pred, lbl_original):
    pred = swap_axes(pred)
    pred_upscale = upscale_segmentation(pred, np.shape(lbl_original))
    return 100 * np.sum(np.equal(pred_upscale, lbl_original)) / np.prod(lbl_original.shape)
